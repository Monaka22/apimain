import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import "firebase/firestore";

const config = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
}
const fire = firebase.initializeApp(config)
const db = fire.firestore()
const storage = fire.storage()

export default firebase
export { db,storage }