import React, { Component } from "react";
import history from "./utils/history";
import { Switch, Route } from "react-router-dom";
import { Router,Redirect } from "react-router";
import {
  Dashboard,
  ImportDomain,
  Login,
  DomainList,
  DomainConfig,
  Installed,
  WpConfig,
} from "./components/pages";

export const Routes = {
  login: "/",
  dashboard: "/dashboard",
  importDomain: "/import-domain",

  domainList: "/domain-list",
  intalled: "/domain-list/installing",

  domainConfig: "/domain-config",
  wpConfig: "/domain-config/wp-config",
};
const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      !localStorage.getItem("auth") ? (
        <Component {...props} />
      ) : (
        <Redirect to="/dashboard" />
      )
    }
  />
);
const SecuredRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem("auth") ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);
class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <LoginRoute exact path={Routes.login} component={Login} />
          <SecuredRoute exact path={Routes.dashboard} component={Dashboard} />
          <SecuredRoute exact path={Routes.importDomain} component={ImportDomain} />

          <SecuredRoute exact path={Routes.domainConfig} component={DomainConfig} />
          <SecuredRoute exact path={Routes.wpConfig} component={WpConfig} />

          <SecuredRoute exact path={Routes.domainList} component={DomainList} />
          <SecuredRoute exact path={Routes.installed} component={Installed} />
        </Switch>
      </Router>
    );
  }
}

export default App;
