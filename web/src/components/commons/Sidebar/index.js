import React from "react";
import PropTypes from "prop-types";
import { Menu } from "antd";
import { NavLink } from "react-router-dom";
import { MenuStyle } from "./style";
// const { SubMenu } = Menu;

const Sidebar = ({ className, defaultLocation, location, menuRoute, mode }) => {
  return (
    <MenuStyle
      className={`border-0 h-100 nav-sidebar ${className}`}
      mode={mode}
      defaultSelectedKeys={[defaultLocation]}
      selectedKeys={[location]}
      // defaultOpenKeys={["sub1"]}
      // inlineCollapsed={this.state.collapsed}
    >
      {menuRoute.map((i, k) => (
        <Menu.Item key={i.key} icon={i.icon}>
          <NavLink to={i.to}>
            <span className="text-capitalize">{i.title}</span>
          </NavLink>
        </Menu.Item>
      ))}
      {/* <SubMenu key="points" icon={<ShopOutlined />} title="Points">
        <Menu.Item key="/manual-add">
          <NavLink to="/manual-add">
            <ShopOutlined />
            <span>Manual Add</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/online-activity">
          <NavLink to="/online-activity">
            <ShopOutlined />
            <span>Online Activity</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/check-in-event">
          <NavLink to="/check-in-event">
            <ShopOutlined />
            <span>Check in event</span>
          </NavLink>
        </Menu.Item>
      </SubMenu> */}
    </MenuStyle>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  menuRoute: PropTypes.array.isRequired,
  location: PropTypes.string,
  mode: PropTypes.string,
  defaultLocation: PropTypes.string,
};
Sidebar.defaultProps = {
  className: "",
};

export default Sidebar;
