import styled from "styled-components";
import { Menu } from "antd";

export const MenuStyle = styled(Menu)`
  &.nav-sidebar {
    .ant-menu-item {
      &.ant-menu-item-selected {
        background-color: #01a69d;
        .anticon {
          color: #ffffff;
        }
        a {
          color: #ffffff;
          &:hover {
            color: #ffffff;
          }
        }
        &::after {
          border-right: 3px solid #01a69d;
        }
      }
      .active {
      }
    }
  }
`;
