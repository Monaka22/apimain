import React, { Component } from "react";
import styled from "styled-components";
import { Breadcrumb, Layout, Menu } from "antd";
import {
  ShopOutlined,
  GlobalOutlined,
  AppstoreOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import Sidebar from "../Sidebar";
import Header from "../../commons/Header";
import logo from "../../../logo.svg";
import  firebase from "../../../config/firebaseConfig";

const { Content, Sider, Footer } = Layout;

const ContentDashboard = styled(Content)`
  height: auto;
  padding: 30px;

  article {
    width: 936px;
    margin: 0 auto;
    &.container-fluid {
      width: 100%;
    }
    @media (max-width: 1280px) {
      width: 100%;
    }
  }
`;

const UserMenu = styled(Menu)`
  .ant-dropdown-menu-item > .anticon {
    font-size: 18px;
  }
`;

class LayoutDashboard extends Component {
  static defaultProps = {
    loading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: 'ADMIN',
      email: JSON.parse(localStorage.getItem('firebaseUser')).data.email ?JSON.parse(localStorage.getItem('firebaseUser')).data.email :"",
      imgSrc:
        "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
      collapsed: false,
    };
  }

  async componentDidMount() {
    firebase.auth().onIdTokenChanged(async function (user) {
      if (user) {
        await user.getIdToken(true).then(function (idToken) {
          localStorage.setItem("auth", idToken);
        });
      }
    });
  }

  logout = () => {
    localStorage.clear()
    window.location.href = "/";
  };

  render() {
    let path = window.location.pathname;
    const host = path.split("/");
    let marp = host.slice(1, host.length);

    const couponOption = marp.map((name, index) => {
      const routeTo = `${marp.slice(0, index + 1).join("/")}`;
      marp[1] = this.props.pageTitle;

      let routesId = host.slice(2, host.length - 1);
      const isSec = `${marp.slice(0, host.length - 2).join("/")}`;
      const routesIdLink = `${host.slice(0, host.length - 2).join("/")}`;
      const isLast = index === marp.length - 1;
      return (
        <Breadcrumb.Item key={index}>
          {marp.length === 2 ? (
            <Link
              disabled={isLast}
              className="text-capitalize text-muted pl-md-2 text-lowercase"
              to={`${
                routeTo === "/" ? `/${routeTo + `/${marp[0]}`}` : `/${routeTo}`
              }`}
            >
              <span>{name && name.split("-").join(" ")}</span>
            </Link>
          ) : (
            <Link
              disabled={isLast}
              className="text-capitalize text-muted pl-md-2 text-lowercase"
              to={
                routeTo !== isSec
                  ? `${routesIdLink}`
                  : `${routesIdLink + `/${routesId}`}`
              }
            >
              <span>{name && name.split("-").join(" ")}</span>
            </Link>
          )}
        </Breadcrumb.Item>
      );
    });
    const menu = (
      <UserMenu>
        <Menu.Item onClick={this.logout} icon={<LogoutOutlined />}>
          <span>Log out</span>
        </Menu.Item>
      </UserMenu>
    );

    let { email, name, imgSrc } = this.state;
    let menuSidebar = [
      {
        key: "/dashboard",
        to: "/dashboard",
        title: "dashboard",
        icon: <ShopOutlined />,
      },
      {
        key: "/import-domain",
        to: "/import-domain",
        title: "import domain",
        icon: <GlobalOutlined />,
      },
      {
        key: "/domain-list",
        to: "/domain-list",
        title: "domain list",
        icon: <GlobalOutlined />,
      },
      {
        key: "/domain-config",
        to: "/domain-config",
        title: "domain config",
        icon: <GlobalOutlined />,
      },
    ];

    return (
      <React.Fragment>
        <Layout>
          <Header
            siteTitle="joe bot"
            linkSite="/dashboard"
            logo={logo}
            collapsedMobile={() => {
              this.setState({
                collapsed: !this.state.collapsed,
              });
            }}
            username={name}
            email={email}
            avatarImg={imgSrc}
            menu={menu}
          />
          <Content>
            <Breadcrumb
              className="p-2 py-md-2 pl-md-4 breadcrumb-wrapper bg-gray-light"
              separator=">"
            >
              <Breadcrumb.Item href="/dashboard">
                <AppstoreOutlined className="ic-large" />
              </Breadcrumb.Item>
              {couponOption}
            </Breadcrumb>
            <Layout>
              <Sider
                breakpoint="lg"
                collapsedWidth="0"
                // onBreakpoint={(broken) => {
                //   console.log(broken);
                // }}
                onCollapse={(collapsed, type) => {
                  // console.log(collapsed, type);
                  this.setState({
                    collapsed: collapsed,
                  });
                }}
                trigger={null}
                style={{ minHeight: "85vh" }}
                collapsible
                collapsed={this.state.collapsed}
              >
                <Sidebar
                  mode="inline"
                  defaultLocation="dashboard"
                  location={path}
                  menuRoute={menuSidebar}
                />
              </Sider>
              <ContentDashboard>
                <article className={this.props.className}>
                  {this.props.children}
                </article>
              </ContentDashboard>
            </Layout>
          </Content>
        </Layout>
        <Footer />
      </React.Fragment>
    );
  }
}

export default LayoutDashboard;
