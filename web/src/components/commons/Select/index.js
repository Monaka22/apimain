import React from "react";
import { Select } from "antd";
import styled from "styled-components";
import PropTypes from "prop-types";

const { Option } = Select;

export const SelectStyle = styled(Select)``;

const SelectOption = ({ className, opt, placeholder, onChange,value }) => {
  return (
    <SelectStyle
      placeholder={placeholder}
      onChange={onChange}
      className={className}
      value = {value}
    >
      {opt.map((item, k) => (
        <Option value={item.id} key={k.toString()}>
          {item.value}
        </Option>
      ))}
    </SelectStyle>
  );
};

SelectOption.propTypes = {
  className: PropTypes.string,
  opt: PropTypes.array.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};
SelectOption.defaultProps = {
  className: "",
};
export default SelectOption;
