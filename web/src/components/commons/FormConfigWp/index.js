import React from "react";
import { Card, Form, Input } from "antd";
import Button from "../Button";
import Checkbox from "../Checkbox";

const FormConfigWp = ({
  className,
  title,
  layoutFrom,
  onFinish,
  onFinishFailed,
  layoutFormItem,
  layoutFormSubmit,
  groupCheckbox,
  optionsCheckbox,
  data,
  formNameCheckbox,
  labelNameCheckbox,
  id,
  onChange,
  oleValue
}) => {
  return (
    <Card
      className={className}
      title={title}
      // extra={<a href="#">More</a>}
    >
      <Form
        {...layoutFrom}
        name={`basic`}
        initialValues={{
          title: oleValue.title ? oleValue.title : '',
          meta_tag : oleValue.meta_tag ? oleValue.meta_tag : '',
          site_name : oleValue.site_name ? oleValue.site_name : ''
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Title"
          name={`title`}
          {...layoutFormItem}
          rules={[
            {
              required: true,
              message: "Please input your title",
            },
          ]}
        >
          <Input
            onChange={(e) =>
              onChange(e.target.value, id, `title`, title)
            }
          />
        </Form.Item>

        <Form.Item
          label="Meta Tag"
          name={`meta_tag`}
          {...layoutFormItem}
          rules={[
            {
              required: true,
              message: "Please input your meta tag",
            },
          ]}
        >
          <Input
            onChange={(e) =>
              onChange(e.target.value, id, `meta_tag`, title)
            }
          />
        </Form.Item>

        <Form.Item
          label="Site Name"
          name={`site_name`}
          {...layoutFormItem}
          rules={[
            {
              required: true,
              message: "Please input your site name",
            },
          ]}
        >
          <Input
            onChange={(e) =>
              onChange(e.target.value, id, `site_name`, title)
            }
          />
        </Form.Item>

        <Checkbox
          formName={formNameCheckbox}
          defaultValue={oleValue.plugin}
          labelName={labelNameCheckbox}
          layout={layoutFormItem}
          group={groupCheckbox}
          options={optionsCheckbox}
          onChange={(value) => {
            console.log(value);
            onChange(value, id, `${formNameCheckbox}`, title);
          }}
          // defaultValue={[1]} onChangeCheck
        />
        {data.length > 1 ? (
          ""
        ) : (
          <Form.Item {...layoutFormSubmit}>
            <Button className="px-4" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        )}
      </Form>
    </Card>
  );
};

export default FormConfigWp;
