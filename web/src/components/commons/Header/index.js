import React from "react";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Avatar, Col, Dropdown, Row } from "antd";
import { HeaderDashboard } from "./style";

const HeaderComponents = ({
  logo,
  siteTitle,
  linkSite,
  collapsedMobile,
  username,
  email,
  avatarImg,
  menu,
}) => {
  return (
    <HeaderDashboard>
      <Row justify="space-between" align="center">
        <Col>
          <div
            className="d-flex d-lg-none flex-row text-white"
            onClick={collapsedMobile}
          >
            <div className="pt-1">
              <img
                className="d-block d-lg-none"
                src={logo}
                alt={siteTitle}
                width={55}
              />
            </div>
            <div
              className="d-block d-lg-none text-white"
              onClick={collapsedMobile}
            >
              {collapsedMobile === true ? (
                <MenuUnfoldOutlined />
              ) : (
                <MenuFoldOutlined />
              )}
            </div>
          </div>

          <Link to={linkSite}>
            <Row justify="space-around">
              <Col className="align-self-lg-center">
                <img
                  className="d-none d-lg-block"
                  src={logo}
                  alt={siteTitle}
                  width={55}
                />
              </Col>
              <Col>
                <h2 className="ml-2 site-title text-capitalize d-none d-lg-block">
                  joe Bot
                </h2>
              </Col>
            </Row>
          </Link>
        </Col>
        <Col>
          <Row gutter={[16, 0]} justify="end">
            <Col>
              <Dropdown trigger={["click"]} overlay={menu}>
                <Row
                  gutter={[8, 0]}
                  align="middle"
                  justify="center"
                  className="user-menu"
                >
                  <Col>
                    <div className="user-profile text-rignt d-none d-md-block">
                      <span>{username}</span>
                      <span>{email}</span>
                    </div>
                  </Col>
                  <Col>
                    <Avatar shape="square" src={avatarImg} size={48} />
                  </Col>
                </Row>
              </Dropdown>
            </Col>
          </Row>
        </Col>
      </Row>
    </HeaderDashboard>
  );
};
export default HeaderComponents;
