import styled from "styled-components";
import { Layout } from "antd";
const { Header } = Layout;

export const HeaderDashboard = styled(Header)`
  padding-left: 1em;
  padding-right: 1em;
  color: #fff;
  background: #01a69d;
  .site-title {
    margin-bottom: 0;
    color: #ffffff !important;
    line-height: 2;
  }
  .ant-avatar.ant-avatar-icon {
    height: auto !important;
    width: auto !important;
    > .anticon {
      font-size: 24px;
      vertical-align: middle !important;
    }
  }
  .text-white {
    color: #ffffff;
  }
  .user-profile {
    height: 100%;
    cursor: pointer;
    &.text-rignt {
      text-align: right;
    }
    span {
      display: block;
      line-height: 1.5;
      &:first-child {
        font-weight: bold;
      }
    }
  }
`;
