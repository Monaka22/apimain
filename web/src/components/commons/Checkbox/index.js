import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Checkbox, Form } from "antd";

const CheckboxComponents = ({
  className,
  group,
  options,
  onChange,
  disabled,
  defaultValue,
  labelName,
  layout,
  formName,
}) => {
  return (
    <>
      {group === true ? (
        <Form.Item
          name={formName}
          {...layout}
          label={labelName}
          initialValue={defaultValue}
        >
          <Checkbox.Group
            disabled={disabled === true ? true : false}
            // defaultValue={defaultValue}
            onChange={onChange}
          >
            <Row className={className}>
              {options.map((item, k) => (
                <Col xs={24} sm={24} md={24} lg={12} xl={8} key={k.toString()}>
                  <Checkbox value={item.id}>{item.name}</Checkbox>
                </Col>
              ))}
            </Row>
          </Checkbox.Group>
        </Form.Item>
      ) : (
        <Form.Item
          name={formName}
          {...layout}
          label={labelName}
          initialValue={defaultValue}
        >
          <Checkbox
            className={className}
            onChange={onChange}
            disabled={disabled === true ? true : false}
          >
            {options}
          </Checkbox>
        </Form.Item>
      )}
    </>
  );
};

CheckboxComponents.propTypes = {
  className: PropTypes.string,
  group: PropTypes.bool.isRequired,
  options: PropTypes.array,
  formName: PropTypes.string.isRequired,
  labelName: PropTypes.string,
  defaultValue: PropTypes.array,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};
CheckboxComponents.defaultProps = {
  className: "",
};
export default CheckboxComponents;
