import React from "react";
import { ProgressStyle } from "./style";

const ProgressBar = (props) => {
  const { bgcolor, completed, end } = props;

  const fillerStyles = {
    height: "100%",
    width: `${completed}%`,
    backgroundColor: bgcolor,
    borderRadius: "inherit",
    textAlign: "right",
  };

  return (
    <div className="progress d-md-flex w-100 text-center">
      <ProgressStyle className="progress-bar">
        <div style={fillerStyles}>
          <span className="progress-count">{`${completed}%`}</span>
        </div>
      </ProgressStyle>
      <span className="ml-2">{end}</span>
    </div>
  );
};

export default ProgressBar;
