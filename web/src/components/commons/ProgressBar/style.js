import styled from "styled-components";

export const ProgressStyle = styled.div`
  overflow: hidden;
  &.progress-bar {
    width: 100%;
    height: 20px;
    border-radius: 50px;
    display: inline-block;
    background-color: #e0e0de;
    @media only screen and (min-width: 1024px) {
      width: 96%;
    }
    @media only screen and (min-width: 1024px) {
      width: 96%;
    }
  }
  .progress-count {
    font-size: 13px;
    color: #ffffff;
    font-weight: bold;
  }
`;
