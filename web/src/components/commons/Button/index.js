import React from "react";
import PropTypes from "prop-types";
import { ButtonStyle } from "./style";

const ButtonDefault = ({
  className,
  onClick,
  children,
  block,
  htmlType,
  disabled,
  loading,
}) => {
  return (
    <ButtonStyle
      className={className}
      size="large"
      onClick={onClick}
      htmlType={htmlType}
      disabled={disabled}
      loading={loading}
      block={block === true ? true : false}
    >
      {children}
    </ButtonStyle>
  );
};

ButtonDefault.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  block: PropTypes.bool,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
};
ButtonDefault.defaultProps = {
  className: "",
};
export default ButtonDefault;
