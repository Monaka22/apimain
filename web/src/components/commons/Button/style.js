import styled from "styled-components";
import { Button } from "antd";

export const ButtonStyle = styled(Button)`
  background: ${(props) => props.theme.primary};
  color: #ffffff;
  border: 2px solid ${(props) => props.theme.primary};
  text-transform: capitalize;
  &:active,
  &:focus {
    background: ${(props) => props.theme.primary};
    color: #ffffff;
    border: 2px solid ${(props) => props.theme.primary};
    text-transform: capitalize;
  }
  &:hover {
    background: #ffffff;
    color: ${(props) => props.theme.primary};
    border: 2px solid ${(props) => props.theme.primary};
  }
`;
