import React from "react";
import { Steps } from "antd";
import PropTypes from "prop-types";
import { StepsStyle } from "./style";

const { Step } = Steps;

const StepHead = ({ className, steps, current }) => {
  return (
    <>
      <StepsStyle className={`mb-4 ${className}`} current={current}>
        {steps.map((item, k) => (
          <Step
            key={item.title}
            title={item.title}
            // description="This is a description."
            icon={<div className="d-none d-md-block">{item.icon}</div>}
          />
        ))}
      </StepsStyle>
    </>
  );
};

StepHead.propTypes = {
  className: PropTypes.string,
  steps: PropTypes.array.isRequired,
  current: PropTypes.number,
};
StepHead.defaultProps = {
  className: "",
};
export default StepHead;
