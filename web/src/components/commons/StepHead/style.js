import styled from "styled-components";
import { Steps } from "antd";

export const StepsStyle = styled(Steps)`
  /* .ant-steps-item {
    display: block;
    min-height: 48px;
    overflow: hidden;
    .ant-steps-item-title {
      position: relative;
      display: inline-block;
      padding-right: 16px;
      color: rgba(0, 0, 0, 0.65);
      font-size: 16px;
      line-height: 32px;
    }
  } */
  &.ant-steps-label-horizontal {
    display: flex;
    flex-direction: column;
    align-items: center;
    @media only screen and (min-width: 1200px) {
      flex-direction: row;
    }
    .ant-steps-item-title {
      padding-right: 0px;
      @media only screen and (min-width: 1200px) {
        padding-right: 16px;
      }
      &::after {
        position: absolute;
        top: 30px;
        left: 50%;
        display: block;
        width: 1px;
        height: 100%;
        background: #f0f0f0;
        content: "";
        @media only screen and (min-width: 768px) {
          left: calc(50% - 20px);
        }
        @media only screen and (min-width: 1200px) {
          top: 16px;
          left: 100%;
          width: 9999px;
          height: 1px;
        }
      }
    }
  }
  &.ant-steps-horizontal {
    &:not(.ant-steps-label-vertical) {
      .ant-steps-item {
        @media only screen and (max-width: 320px) {
          padding-left: 16px !important;
        }
        @media only screen and (min-width: 400px) {
          padding-left: 0 !important;
        }
        @media only screen and (min-width: 1200px) {
          padding-left: 16px !important;
        }
        &:first-child {
          @media only screen and (max-width: 320px) {
            padding-left: 16px !important;
          }
          @media only screen and (min-width: 400px) {
            padding-left: 0 !important;
          }
        }
      }
    }
    &.ant-steps-label-horizontal {
      .ant-steps-item-content {
        min-height: 60px;
        text-align: center;
        @media only screen and (min-width: 576px) {
          left: 16px;
          min-height: 48px;
        }
        @media only screen and (min-width: 768px) {
          left: 0;
          min-height: 48px;
        }
      }
      > .ant-steps-item {
        > .ant-steps-item-container {
          > .ant-steps-item-tail {
            left: 50%;
            @media only screen and (min-width: 576px) {
              left: 16px;
            }
            @media only screen and (min-width: 768px) {
              left: 0;
            }
            &::after {
              background-color: rgba(0, 0, 0, 0.25);
            }
          }
        }
      }
      .ant-steps-item-finish {
        > .ant-steps-item-container {
          > .ant-steps-item-tail::after {
            background-color: #1890ff;
          }
        }
      }
    }
  }
`;
