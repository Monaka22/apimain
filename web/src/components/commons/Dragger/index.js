import React from "react";
import { Upload } from "antd";
import PropTypes from "prop-types";
import { InboxOutlined } from "@ant-design/icons";
const { Dragger } = Upload;

const Drag = ({ name, multiple, listType, beforeUpload, onChange, action }) => {
  return (
    <Dragger
      name={name}
      multiple={multiple}
      action={action}
      listType={listType}
      beforeUpload={beforeUpload}
      onChange={onChange}
    >
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">
        Click or drag file to this area to upload
      </p>
      <p className="ant-upload-hint">
        Support for a single or bulk upload. Strictly prohibit from uploading
        company data or other band files
      </p>
    </Dragger>
  );
};

Drag.propTypes = {
  name: PropTypes.string,
  multiple: PropTypes.bool,
  action: PropTypes.string,
  listType: PropTypes.string,
  beforeUpload: PropTypes.func,
  onChange: PropTypes.func,
};
Drag.defaultProps = {
  name: "",
};

export default Drag;
