import styled from "styled-components";
import Layout from "../../commons/Layouts";

export const ImportDomainStyle = styled(Layout)`
  &.domain {
    .anticon.ic-large {
      font-size: 40px !important;
    }
    .ant-steps {
      &.ant-steps-horizontal {
        &.ant-steps-label-horizontal {
          @media only screen and (max-width: 768) {
            display: flex;
            flex-direction: column;
          }
        }
      }
    }
  }
`;
