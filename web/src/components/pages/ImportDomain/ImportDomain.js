import React, { Component } from "react";
import { ImportDomainStyle } from "./style";
import { message, Card, Form, Input, Table } from "antd";
import {
  UserOutlined,
  IdcardOutlined,
  SmileOutlined,
  CloudUploadOutlined,
  CloudSyncOutlined,
  TableOutlined,
} from "@ant-design/icons";
import Dragger from "../../commons/Dragger";
import Swal from "sweetalert2";
// import moment from "moment";
import xlsxParser from "xlsx-parse-json";
import ProgressBar from "../../commons/ProgressBar";
import Button from "../../commons/Button";
import StepHead from "../../commons/StepHead";
import Select from "../../commons/Select";
import { httpClient } from '../../../utils/HttpClient'

const steps = [
  {
    title: "Select Hosting",
    key: 1,
    icon: <UserOutlined className="ic-large" />,
  },
  {
    title: "login hosting",
    key: 2,
    icon: <IdcardOutlined className="ic-large" />,
  },
  {
    title: "Upload File",
    key: 3,
    icon: <CloudUploadOutlined className="ic-large" />,
  },
  {
    title: "Select domain",
    key: 4,
    icon: <CloudSyncOutlined className="ic-large" />,
  },
  {
    title: "Preview domain",
    key: 5,
    icon: <TableOutlined className="ic-large" />,
  },
  {
    title: "Last Installing",
    key: 6,
    icon: <SmileOutlined className="ic-large" />,
  },
];
function keysToLowerCase(obj) {
  if(obj instanceof Array) {
      for (var i in obj) {
          obj[i] = keysToLowerCase(obj[i]);
      }
  }
  if (!typeof(obj) === "object" || typeof(obj) === "string" || typeof(obj) === "number" || typeof(obj) === "boolean") {
      return obj;
  }
  var keys = Object.keys(obj);
  var n = keys.length;
  var lowKey;
  while (n--) {
      var key = keys[n];
      if (key === (lowKey = key.toLowerCase()))
          continue;
      obj[lowKey] = keysToLowerCase(obj[key]);
      delete obj[key];
  }
  return (obj);
}
class ImportDomain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      select: "",
      username: "",
      password: "",
      files: [],
      selectedRowKeys: [],
      selectedRows: [],
      columns: [],
      percent: 0,
      host:[]
    };
  }
  componentDidMount(){
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    this.fetchData()
  }
  fetchData=()=>{
    httpClient.get(`host/host-all`).then(doc=>{
      let data = []
      doc.data.data.forEach(ele=>{
        data.push(Object.assign({
          id : ele.id,
          value: ele.host_name
        }))
      })
      this.setState({host:data})
      Swal.close()
    }).catch(err=>{throw err})
  }
  next = async () => {
    let {
      current,
      select,
      username,
      password,
      files,
      columns,
      selectedRows,
    } = this.state;
    const currentStaps = current + 1;
    if (current === 0 && select !== "") {
      this.setState({ current: currentStaps });
    } else if (current === 1 && username !== "" && password !== "") {
      this.setState({ current: currentStaps });
    } else if (current === 2 && files.length > 0) {
      this.setState({ current: currentStaps });
    } else if (current === 3 && columns.length > 0) {
      this.setState({ current: currentStaps });
    } else if (current === 4 && selectedRows.length > 0) {
      this.setState({ current: currentStaps });
      this.myVar = setInterval(this.percentChange, 1000);
      await this.setState({
        percent : 0
      })
      this.onFinish()
    } else {
      Swal.fire({
        icon: "error",
        title: "Please enter your data",
        timer: 1500,
        // text: "Something went wrong!",
        // footer: "<a href>Why do I have this issue?</a>",
      });
    }
  };

  myStopFunction = () => {
    clearInterval(this.myVar);
  }
  prev = () => {
    let { current } = this.state;
    const currentStaps = current - 1;
    this.setState({ current: currentStaps });
  };
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  percentChange = () => {
    let percent = this.state.percent + 1;
    if (percent > 100) {
      clearInterval(this.myVar);
      percent = 100;
      Swal.fire({
        icon: "success",
        title: "Successfully installed",
        timer: 1500,
      })
      window.location.reload()
    }
    this.setState({ percent });
  };

  onFinish = (domains) => {
    let {selectedRows,select,password,username} = this.state
    let data = []
    selectedRows.forEach(doc=>{
      data.push(Object.assign({
        host_domain_name : doc.name,
        host_id : +select,
      }))
    })
    httpClient.post(`host/domain-create`,{
      domains : data,
      user_id : JSON.parse(localStorage.getItem('firebaseUser')).id
    }).then(res=>{
      this.setState({
        percent : 100
      })
    }).catch(err=>{throw err})
    httpClient.post(`host/user-create`,{
      host_user_username : username,
      host_user_password : password,
      host_id:+select
    }).then(res=>{}).catch(err=>{throw err})
  };

  handleChange = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      select: value,
    });
  };

  onUplodChange = (info) => {
    const { status } = info.file;
    if (status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
      xlsxParser.onFileSelection(info.file.originFileObj).then(async (data) => {
        // console.log("data", info.file.name);
        let res = info.file.name.substring(
          info.file.name.length - 4,
          info.file.name.length
        );
        let firstSheet = Object.keys(data)[0];
        let docData = keysToLowerCase(data[firstSheet]);
        let all = [];
        let j = 0;
        let key = Object.keys(docData[0]);
        docData.forEach((element) => {
          j = j + 1;
          element.key = j
          all.push({
            key: j,
            [key]: element[key],
          });
        });
        // console.log("all", all);
        this.setState({
          files: res === ".csv" ? data[firstSheet] : all,
        });
        // console.log("key", firstKey);
        let keys = [{ title: "key", dataIndex: "key", className: "d-none" }];
        let i;
        for (i = 0; i < key.length; i++) {
          keys.push({
            title: key[i].toLowerCase(),
            dataIndex: key[i].toLowerCase(),
          });
        }
        this.setState({
          columns: keys,
        });
        // console.log("key to column", this.state.columns);
      });
    } else if (status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  beforeUpload = (file) => {
    // console.log(file);
    let res = file.name.substring(file.name.length - 4, file.name.length);
    // console.log(res);
    const isXLSX = res === ".xls" || res === "xlsx" || res === ".csv";
    if (!isXLSX) {
      message.error("You can only upload XLSX/XLS file!");
    }
    return isXLSX;
  };

  render() {
    const { current, files, selectedRowKeys, selectedRows } = this.state;

    const rowSelection = {
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(
          `selectedRowKeys: ${selectedRowKeys}`,
          "selectedRows: ",
          selectedRows
        );
        this.setState({
          selectedRowKeys: selectedRowKeys,
          selectedRows: selectedRows,
        });
      },
      getCheckboxProps: (record) => ({
        disabled: record.name === "Disabled User", // Column configuration not to be checked
        name: record.name,
      }),
    };

    const testData = [{ bgcolor: "#0c7973", completed: this.state.percent }];
    const {host,select,username,password} = this.state;

    return (
      <ImportDomainStyle className="container-fluid domain">
        <StepHead steps={steps} current={current} />
        <Form name="register" onFinish={this.onFinish} scrollToFirstError>
          {steps[current].key === 1 ? (
            <Card className="mb-4 p-0 p-md-4">
              <h3 className="pb-3">Select Hosting</h3>
              <Form.Item
                className="steps-form mb-0"
                label="select hosting"
                name="select"
                hasFeedback
                rules={[
                  { required: true, message: "Please select your country!" },
                ]}
              >
                <Select
                  value={select && select}
                  opt={host}
                  placeholder="Please select web hosting"
                  onChange={this.handleChange}
                />
              </Form.Item>
            </Card>
          ) : steps[current].key === 2 ? (
            <Card className="mb-4 p-0 p-md-4">
              <h3 className="pb-3">Hosting: {this.state.select}</h3>
              <Form.Item
                className="steps-form"
                label="username"
                name="username"
                initialValue= {username}
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input
                  placeholder='input your username'
                  onChange={(e) => this.setState({ username: e.target.value })}
                />
              </Form.Item>
              <Form.Item
                className="steps-form"
                name="password"
                label="password"
                initialValue= {password}
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password
                  placeholder='input your password'
                  onChange={(e) => this.setState({ password: e.target.value })}
                />
              </Form.Item>
            </Card>
          ) : steps[current].key === 3 ? (
            <Card className="mb-4 p-0 p-md-4">
              <Dragger
                name="file"
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                multiple={false}
                listType="text"
                beforeUpload={this.beforeUpload}
                onChange={this.onUplodChange}
              />
            </Card>
          ) : steps[current].key === 4 ? (
            <Card className="mb-4 p-0 p-md-4">
              {files.length > 0 && (
                <Table
                  // rowKey={(record) => record.id}
                  rowSelection={rowSelection}
                  columns={this.state.columns}
                  dataSource={files}
                  pagination={false}
                />
              )}
            </Card>
          ) : steps[current].key === 5 ? (
            <Card className="mb-4 p-0 p-md-4">
              {files.length > 0 && (
                <Table
                  // rowKey={(record) => record.id}
                  columns={this.state.columns}
                  dataSource={selectedRows}
                  pagination={false}
                />
              )}
            </Card>
          ) : (
            <Card className="mb-4 p-0 p-md-4">
              {testData.map((item, idx) => (
                <ProgressBar
                  key={idx}
                  bgcolor={item.bgcolor}
                  completed={item.completed}
                  end="100%"
                />
              ))}
            </Card>
          )}
          <div className="steps-action">
            {current < steps.length - 1 && (
              <Button className="px-4" onClick={async() => this.next()}>
                Next
              </Button>
            )}
            {current === steps.length - 1 && (
              <Button
                className="px-4"
                htmlType="submit"
                onClick={()=>{
                  window.location.reload()
              }}
              >
                Done
              </Button>
            )}
            {/* {current > 0 && current !== steps.length - 1 && ( */}
            {current > 0 && (
              <Button className="ml-2 btn-gray" onClick={() => this.prev()}>
                Previous
              </Button>
            )}
          </div>
        </Form>
      </ImportDomainStyle>
    );
  }
}

export default ImportDomain;
