import React, { Component } from "react";
import { Table } from "antd";
import { DomainListStyle } from "./style";
import Button from "../../commons/Button";
import { httpClient } from '../../../utils/HttpClient'
import Swal from "sweetalert2";

class DomainList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      selectedAll: false,
      data:[],
      total:0
    };
  }

  componentDidMount = () =>{
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    this.fetchData()
  }
  fetchData = () =>{
    httpClient.get(`host/domain/${JSON.parse(localStorage.getItem('firebaseUser')).id}/0/10`).then(doc=>{
      this.setState({
        data:doc.data.data,
        total:doc.data.total
      })
      Swal.close()
    }).catch(err=>{throw err})
  }

  onPageChange = (page, length) =>{
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    httpClient.get(`host/domain/${JSON.parse(localStorage.getItem('firebaseUser')).id}/${(page-1) * length}/10`).then(doc=>{
      this.setState({
        data:doc.data.data,
        total:doc.data.total
      })
      Swal.close()
    }).catch(err=>{throw err})
  }
  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys });
  };
  goToInstallId = (id) => {
    console.log(id);
    this.props.history.push("/domain-list/installing");
  };
  goToInstall = () => {
    this.props.history.push("/domain-list/installing");
  };

  render() {
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onSelectAll: this.onSelectAll,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    const columns = [
      {
        title: "Doamin",
        dataIndex: "host_domain_name",
      },
      {
        title: "Action",
        dataIndex: "",
        key: "x",
        width: "250px",
        className: "text-center",
        render: (text, record) => (
          <Button
            className="px-4"
            onClick={() => this.goToInstallId(record.key)}
          >
            install wordpress
          </Button>
        ),
      },
    ];

    const {data,total} = this.state
    return (
      <DomainListStyle className="domain-list">
        <div className="domain-list__action mb-4">
          <Button
            className="px-2 px-md-4"
            loading={loading}
            disabled={!hasSelected}
            onClick={this.goToInstall}
          >
            install
          </Button>
          <span className="ml-3">
            {hasSelected ? `Selected ${selectedRowKeys.length} domains` : ""}
          </span>
        </div>
        <Table
          rowKey={'id'}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={{
            showQuickJumper: false,
            total: total,
            onChange: (page, length) => {
              this.onPageChange(page, length)
            }
          }}
    
        />
      </DomainListStyle>
    );
  }
}

export default DomainList;
