import styled from "styled-components";
import Layout from "../../commons/Layouts";

export const DomainConfigStyle = styled(Layout)`
  &.domain-config {
    .ant-btn-primary {
      height: auto;
      padding: 3px 14px;
      background: ${(props) => props.theme.primary};
      color: ${(props) => props.theme.white};
      border: 2px solid ${(props) => props.theme.primary};
      text-transform: capitalize;
      &.disabled {
        color: rgba(0, 0, 0, 0.25);
        background: #f5f5f5;
        border-color: #d9d9d9;
        cursor: not-allowed;
        &:active,
        &:focus,
        &:hover {
          color: rgba(0, 0, 0, 0.25);
          background: #f5f5f5;
          border-color: #d9d9d9;
          cursor: not-allowed;
        }
      }
      &:active,
      &:focus {
        background: ${(props) => props.theme.primary};
        color: ${(props) => props.theme.white};
        border: 2px solid ${(props) => props.theme.primary};
        text-transform: capitalize;
      }
      &:hover {
        background: ${(props) => props.theme.white};
        color: ${(props) => props.theme.primary};
        border: 2px solid ${(props) => props.theme.primary};
      }
    }
  }
`;
