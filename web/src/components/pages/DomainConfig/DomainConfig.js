import React, { Component } from "react";
import { Table } from "antd";
import { Link } from "react-router-dom";
import { DomainConfigStyle } from "./style";
import { httpClient } from '../../../utils/HttpClient'
import Swal from "sweetalert2";
class DomainConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      selectedAll: false,
      selectedRows: [],
      data:[],
      total:0
    };
  }
  componentDidMount = () =>{
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    this.fetchData()
  }
  fetchData = () =>{
    httpClient.get(`host/domain/${JSON.parse(localStorage.getItem('firebaseUser')).id}/0/10`).then(doc=>{
      this.setState({
        data:doc.data.data,
        total:doc.data.total
      })
      Swal.close()
    }).catch(err=>{throw err})
  }
  onSelectChange = (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
    this.setState({ selectedRowKeys, selectedRows });
  };
  onSelectAll = (selected, selectedRows, changeRows) => {
    console.log(selected, selectedRows, changeRows);
    this.setState({
      selectedAll: true,
    });
  };
  goToConfigMore = (selectedRows) => {
    console.log("config", selectedRows);
  };
  onPageChange = (page, length) =>{
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    httpClient.get(`host/domain/${JSON.parse(localStorage.getItem('firebaseUser')).id}/${(page-1) * length}/10`).then(doc=>{
      this.setState({
        data:doc.data.data,
        total:doc.data.total
      })
      Swal.close()
    }).catch(err=>{throw err})
  }
  goToConfig = (id) => {
    console.log("config", id);
    this.props.history.push("/domain-config/wp-config");
  };
  render() {
    const { selectedRowKeys, selectedRows } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onSelectAll: this.onSelectAll,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "Id",
        dataIndex: "id",
        className: "d-none",
      },
      {
        title: "Doamin",
        dataIndex: "host_domain_name",
      },
      {
        title: "Action",
        dataIndex: "",
        key: "x",
        render: (text, record) => (
          <Link
            className="ant-btn px-4 ml-auto ant-btn-primary"
            to={{
              pathname: "/domain-config/wp-config",
              state: {
                wpconfig: [record],
              },
            }}
          >
            config
          </Link>
        ),
      },
    ];
    const {data,total} = this.state
    return (
      <DomainConfigStyle
        pageTitle="Domain config"
        className="container domain-config"
      >
        <div className="domain-config__action mb-4">
          <Link
            className={`ant-btn px-4 ml-auto ant-btn-primary ${
              !hasSelected ? "disabled" : ""
            }`}
            to={{
              pathname: "/domain-config/wp-config",
              state: {
                wpconfig: selectedRows,
              },
            }}
          >
            config
          </Link>
          <span className="ml-3">
            {hasSelected ? `Selected ${selectedRowKeys.length} domains` : ""}
          </span>
        </div>
        <Table
          rowKey={'id'}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={{
            showQuickJumper: false,
            total: total,
            onChange: (page, length) => {
              this.onPageChange(page, length)
            }
          }}
        />
      </DomainConfigStyle>
    );
  }
}

export default DomainConfig;
