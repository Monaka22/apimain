import styled from "styled-components";

export const ContainerLoginStyle = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  padding: 15px;
  background: #808080;
  background: linear-gradient(-135deg, #d3ced1, #a9a9a9);
  .wrap-login {
    width: 960px;
    background: #fff;
    border-radius: 10px;
    overflow: hidden;

    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    /* padding: 177px 130px 33px 95px; */
    padding: 33px 130px 33px 95px;
  }
  /*  */
  .login-pic {
    width: 316px;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    img {
      max-width: 100%;
    }
  }
  /*  */
  .login-form {
    width: 290px;
  }

  .login-form-title {
    font-size: 24px;
    color: #333333;
    line-height: 1.2;
    text-align: center;

    width: 100%;
    display: block;
    padding-top: 24px;
    padding-bottom: 54px;
  }
  /*  */
  .wrap-input {
    position: relative;
    width: 100%;
    z-index: 1;
    margin-bottom: 10px;
  }

  .focus-input {
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    z-index: -1;
    width: 100%;
    height: 100%;
    box-shadow: 0px 0px 0px 0px;
    color: rgba(87, 184, 70, 0.8);
  }

  .input {
    color: #666666;
    background: #e6e6e6;
    font-size: 16px;
    &:focus + .focus-input {
      -webkit-animation: anim-shadow 0.5s ease-in-out forwards;
      animation: anim-shadow 0.5s ease-in-out forwards;
    }
    .ant-input {
      background: #e6e6e6;
    }
  }

  /*  */
  .symbol-input {
    font-size: 15px;

    display: flex;
    align-items: center;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    padding-left: 35px;
    pointer-events: none;
    color: #666666;

    transition: all 0.4s;
  }

  .input:focus + .focus-input + .symbol-input {
    color: #57b846;
    padding-left: 28px;
  }
  /* Button */
  .container-login-form-btn {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding-top: 20px;
  }

  .login-form-btn {
    font-size: 15px;
    line-height: 1.5;
    color: #fff;
    text-transform: uppercase;

    width: 100%;
    height: 50px;
    background: #57b846;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.4s;
    &:hover {
      background: #333333;
    }
  }

  .p-t-12 {
    padding-top: 12px;
  }
  .p-t-136 {
    padding-top: 136px;
  }
  .txt1 {
    font-size: 13px;
    line-height: 1.5;
    color: #999999;
  }

  .txt2 {
    font-size: 13px;
    line-height: 1.5;
    color: #666666;
  }
  .fn-100 {
    font-size: 100px;
  }
`;
