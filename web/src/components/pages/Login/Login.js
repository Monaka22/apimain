import React, { Component } from "react";
import {
  BoxPlotOutlined,
  LockOutlined,
  MailOutlined,
  // SwapRightOutlined,
} from "@ant-design/icons";
import { Form, Input } from "antd";
import logo from "../../../logo.svg";
import Button from "../../commons/Button";
// import { Link } from "react-router-dom";
import { ContainerLoginStyle } from "./style";
import firebase from "../../../config/firebaseConfig";
import Swal from 'sweetalert2'
import { httpClient } from '../../../utils/HttpClient'

class Login extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       button : false,
       email : 'dojisjis22@gmail.com',
       password : 'qwerty5678'
    }
  }
  
  onClickLogin = () => {
    setTimeout(() => {
      this.props.history.push("/dashboard");
    }, 1500)
  };
  onFinish = (values) => {
    let that = this
    this.setState({button:true})
    firebase
        .auth()
        .signInWithEmailAndPassword(values.email, values.password)
        .then((response) => {
          Swal.fire({
            icon: "success",
            title: `Logged In!`,
            message: "ระบบกำลังนำคุณไปยังหน้าแดชบอร์ด",
            timer: 1500,
          });
          if (response.user) {
            response.user.getIdToken(true).then(function (idToken) {
              httpClient.post('/users/login',
                    {
                      email:values.email,
                      password : values.password
                    }).then(doc=>{
                      that.setState({button:false})
                      localStorage.setItem("firebaseUser",JSON.stringify({data : response.user,id :doc.data.data.message.id}));
                      localStorage.setItem("auth", idToken);
                      that.onClickLogin();
                    }).catch(err=>{throw err})
            });
          }
        })
        .catch((error) => {
          Swal.fire({
            icon: "error",
            // title: `${error.response.data.message}`,
            title: `ไม่สามารถเข้าสู่ระบบได้`,
            timer: 1500,
          });
          this.setState({ loading: false });
        });
    // this.onClickLogin();
  };
  render() {
    return (
      <ContainerLoginStyle>
        <div className="wrap-login">
          <div className="login-pic">
            <img src={logo} alt="IMG logo" />
            <BoxPlotOutlined className="fn-100" />
          </div>
          <Form
            name="basic"
            className="login-form validate-form"
            // initialValues={{ remember: true }}
            onFinish={this.onFinish}
            // onFinishFailed={onFinishFailed}
          >
            <span className="login-form-title">Member Login</span>
            {/* Input Email */}
            <Form.Item
              initialValue={this.state.email}
              className="wrap-input validate-input"
              name="email"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: "Please input your email!",
                },
              ]}
              placeholder="Email"
            >
              <Input prefix={<MailOutlined />} className="input" />
            </Form.Item>
            {/* Input password */}
            <Form.Item
              initialValue={this.state.password}
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
              placeholder="Password"
            >
              <Input.Password prefix={<LockOutlined />} className="input" />
            </Form.Item>

            <Form.Item className="container-login-form-btn">
              <Button className="px-4" htmlType="submit" block={true} loading={this.state.button}>
                Done
              </Button>
            </Form.Item>
          </Form>
        </div>
      </ContainerLoginStyle>
    );
  }
}

export default Login;
