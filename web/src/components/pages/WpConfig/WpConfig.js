import React, { Component } from "react";
import Layout from "../../commons/Layouts";
import Button from "../../commons/Button";
import FormConfigWp from "../../commons/FormConfigWp";
import Swal from "sweetalert2";
import { httpClient } from '../../../utils/HttpClient'

function removeA(arr) {
  var what,
    a = arguments,
    L = a.length,
    ax;
  while (L > 1 && arr.length) {
    what = a[--L];
    while ((ax = arr.indexOf(what)) !== -1) {
      arr.splice(ax, 1);
    }
  }
  return arr;
}
function count(obj) {
  var count = 0;
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      ++count;
    }
  }
  return count;
}
class WpConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      selectedAll: false,
      data: [],
      domain: [],
      plugins:[]
    };
  }
  componentDidMount() {
    Swal.fire({
      title: "กำลังโหลด",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    const { wpconfig } = this.props.location.state;
    let id = []
    let name = []
    wpconfig.forEach(doc=>{
      id.push(doc.id)
      name.push({
       id:doc.id,
       name: doc.host_domain_name,
       plugin:[]
      })
    })
    this.fetchData(id,name)
  }
  fetchData = (id,name)=>{
    httpClient.post(`config/get`,{domain_id:id}).then(doc=>{
      let data = doc.data.data
      let plugin = data.plugin
      let result = []
      data.config.forEach(ele=>{
        result.push(Object.assign({
          title : ele.domain_config_title,
          site_name : ele.domain_config_site_name,
          meta_tag: ele.domain_config_meta_tag,
          id:ele.domain_id
        }))
        for (let i = 0; i < result.length; i++) {
          let configPlugin = []
          for (let j = 0; j < plugin.length; j++) {
            if(result[i].id === plugin[j].domain_id){
              configPlugin.push(plugin[j].plugin_id)
              result[i].plugin = configPlugin
            }
          }
        }
        for (let i = 0; i < result.length; i++) {
          let nameCon = []
          for (let j = 0; j < name.length; j++) {
            if(result[i].id === name[j].id){
              nameCon.push(name[j].name)
              result[i].name = nameCon[0]
            }
          }
        }
      })
      let unResutl = []
      for (let j = 0; j < name.length; j++) {
        if (result.length > 0) {
          for (let i = 0; i < result.length; i++) {
            if (result[i].id !== name[j].id) {
             unResutl.push(name[j])
            }
          }
        }else{
          unResutl.push(name[j])
        }
      }
      let unique = this.getUnique(result.concat(unResutl), "id")
      this.setState({
        data: unique.sort(function(a,b){return a.id - b.id})
      });
      httpClient.get(`plugin/all`).then(doc=>{
        let result = []
        doc.data.data.forEach(e=>{
          result.push(Object.assign({
            id  : e.id ,
            name :  e.plugin_name
          }))
        })
        this.setState({
          plugins:result
        })
        Swal.close()
      }).catch(err=>{throw err})
      Swal.close()
    }).catch(err=>{throw err})
  }
  onFinish = (values, doc) => {
    let array = [doc]
    let data = []
      array.forEach(doc=>{
          data.push(Object.assign({
            domain_id : doc.id,
            domain_config_title: doc.title,
            domain_config_meta_tag:doc.meta_tag,
            domain_config_site_name:doc.site_name,
            plugin:doc.plugin
          })
        )
      })
      httpClient.post(`config/create`,{data}).then(res=>{
        Swal.fire({
          icon: "success",
          title: "Successfully already exists",
          timer: 1500,
        }).then(() => {
          this.props.history.push("/domain-config");
        });
        Swal.close()
      }).catch(err=>{throw err})
  };

  onChange = async (value, id, colum, domain) => {
    let { data } = this.state;
    if (data.length === 0) {
      data.push({
        id: id,
        [colum]: value,
        name: domain,
      });
    } else {
      data.forEach((docData) => {
        if (docData.id === id) {
          docData[colum] = value;
        } else {
          data.push({
            id: id,
            [colum]: value,
            name: domain,
          });
        }
      });
      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < data.length; j++) {
          if (data[i].id === data[j].id) {
            if (count(data[i]) > count(data[j])) {
              removeA(data, data[j]);
            }
          }
        }
      }
    }
    data = this.getUnique(data, "id") 

    this.setState({ data });
  };
  getUnique(arr, comp) {
    //store the comparison  values in array
    const unique = arr
      .map((e) => e[comp])
      // store the indexes of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the false indexes & return unique objects
      .filter((e) => arr[e])
      .map((e) => arr[e]);

    return unique;
  }

  onConfigAll = async(data) => {
    let compls = true
    await data.forEach((doc,key)=>{
      console.log(key)
      if(!doc.meta_tag){
        return Swal.fire({
              icon: "error",
              title: `กรุณากรอกข้อมูลให้ครบถ้วน`,
              timer: 1500,
            }).then(() => {
              compls = false
            });
      }
      if(!doc.site_name){
        return Swal.fire({
          icon: "error",
          title: `กรุณากรอกข้อมูลให้ครบถ้วน`,
          timer: 1500,
        }).then(() => {
          compls = false
        });
      }
      if(!doc.title){
        return Swal.fire({
          icon: "error",
          title: `กรุณากรอกข้อมูลให้ครบถ้วน`,
          timer: 1500,
        }).then(() => {
          compls = false
        });
      }
      key+1 === data.length && this.checkData(compls,data)
    })
  };
  checkData = (compls,array)=>{
    if(compls === true){
      Swal.fire({
        title: "กำลังโหลด",
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
          Swal.showLoading();
        }
      });
      let data = []
      array.forEach(doc=>{
          data.push(Object.assign({
            domain_id : doc.id,
            domain_config_title: doc.title,
            domain_config_meta_tag:doc.meta_tag,
            domain_config_site_name:doc.site_name,
            plugin:doc.plugin
          })
        )
      })
      httpClient.post(`config/create`,{data}).then(res=>{
        Swal.fire({
          icon: "success",
          title: "Successfully already exists",
          timer: 1500,
        }).then(() => {
          this.props.history.push("/domain-config");
        });
        Swal.close()
      }).catch(err=>{throw err})
    }
  }
  render() {
    let { data,plugins } = this.state;
    console.log(data)
    const layout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 20 },
    };
    const tailLayout = {
      wrapperCol: { span: 20 },
    };
    const submitLayout = {
      wrapperCol: { span: 20, offset: 4 },
    };

    return (
      <Layout
        pageTitle="Wordpress config"
        className="container-fluid wordpress-config"
      >
        {data.length > 1 ? (
          <div className="d-flex mb-4">
            <Button
              className="px-4 ml-auto"
              onClick={() => this.onConfigAll(data)}
            >
              config all
            </Button>
          </div>
        ) : (
          ""
        )}
        {/* {domain.length > 1 ? ( */}
          {data.map((item, k) => (
            <FormConfigWp
              onChange={this.onChange}
              key={`${k}`}
              className="mb-4 p-0"
              oleValue = {item}
              title={item.name}
              id={item.id}
              data={data}
              layoutFrom={layout}
              onFinish={(value) => this.onFinish(value, item)}
              layoutFormItem={tailLayout}
              layoutFormSubmit={submitLayout}
              formNameCheckbox="plugin"
              labelNameCheckbox="Plugin"
              groupCheckbox={true}
              optionsCheckbox={plugins}
              // initialValues={}
            />
          ))}
        {/* ) : ( */}
          {/* <FormConfigWp
            onChange={this.onChange}
            className="mb-4 p-0"
            title={domain.name}
            data={domain}
            layoutFrom={layout}
            onFinish={(value) => this.onFinish(value, domain.key)}
            layoutFormItem={tailLayout}
            layoutFormSubmit={submitLayout}
            formNameCheckbox="plugin"
            labelNameCheckbox="Plugin"
            groupCheckbox={true}
            optionsCheckbox={plugins}
            // initialValues={}
          />
        )} */}
      </Layout>
    );
  }
}

export default WpConfig;
