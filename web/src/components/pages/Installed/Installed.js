import React, { Component } from "react";
import Layout from "../../commons/Layouts";
import { Card } from "antd";
import ProgressBar from "../../commons/ProgressBar";
import Swal from "sweetalert2";
class Installed extends Component {
  constructor(props) {
    super(props);
    this.tick = this.tick.bind(this);
    this.state = {
      seconds: 100,
      percent: 0,
    };
  }

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  tick() {
    // for auto
    let percent = this.state.percent + 1;
    if (this.state.seconds > 0) {
      this.setState({
        seconds: this.state.seconds - 1,
        percent: percent,
      });
      this.setState({ percent });
    } else {
      if (percent > 100) {
        percent = 100;
        Swal.fire({
          icon: "success",
          title: "Successfully installed",
          timer: 1500,
          // text: "Something went wrong!",
          // footer: "<a href>Why do I have this issue?</a>",
        }).then(() => {
          this.props.history.push("/domain-list");
        });
      }
      clearInterval(this.timer);
      // window.location.reload();
    }
  }

  render() {
    let { percent } = this.state;
    return (
      <Layout pageTitle="installing" className="container-fluid installed">
        <Card>
          {/* {this.state.seconds} */}
          <ProgressBar bgcolor="#0c7973" completed={percent} end="100%" />
        </Card>
      </Layout>
    );
  }
}
export default Installed;
