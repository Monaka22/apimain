/*
 Navicat Premium Data Transfer

 Source Server         : postgras
 Source Server Type    : PostgreSQL
 Source Server Version : 120003
 Source Host           : 0.0.0.0:5432
 Source Catalog        : domainAPI
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120003
 File Encoding         : 65001

 Date: 21/08/2020 13:59:59
*/


-- ----------------------------
-- Sequence structure for Users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Users_id_seq";
CREATE SEQUENCE "public"."Users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."Users_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for domain_configs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."domain_configs_id_seq";
CREATE SEQUENCE "public"."domain_configs_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."domain_configs_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for domain_plugins_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."domain_plugins_id_seq";
CREATE SEQUENCE "public"."domain_plugins_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."domain_plugins_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for host_domains_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."host_domains_id_seq";
CREATE SEQUENCE "public"."host_domains_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."host_domains_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for host_users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."host_users_id_seq";
CREATE SEQUENCE "public"."host_users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."host_users_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for hosts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."hosts_id_seq";
CREATE SEQUENCE "public"."hosts_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."hosts_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Sequence structure for plugins_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."plugins_id_seq";
CREATE SEQUENCE "public"."plugins_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."plugins_id_seq" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Table structure for SequelizeMeta
-- ----------------------------
DROP TABLE IF EXISTS "public"."SequelizeMeta";
CREATE TABLE "public"."SequelizeMeta" (
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;
ALTER TABLE "public"."SequelizeMeta" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of SequelizeMeta
-- ----------------------------
BEGIN;
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811060508-create-hosts.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811060715-create-host-users.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811061315-create-host-domains.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811061659-create-domain-configs.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811061807-create-plugins.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200811061906-create-domain-plugins.js');
INSERT INTO "public"."SequelizeMeta" VALUES ('20200817032548-create-users.js');
COMMIT;

-- ----------------------------
-- Table structure for domain_configs
-- ----------------------------
DROP TABLE IF EXISTS "public"."domain_configs";
CREATE TABLE "public"."domain_configs" (
  "id" int4 NOT NULL DEFAULT nextval('domain_configs_id_seq'::regclass),
  "domain_config_title" varchar(255) COLLATE "pg_catalog"."default",
  "domain_config_meta_tag" varchar(255) COLLATE "pg_catalog"."default",
  "domain_config_site_name" varchar(255) COLLATE "pg_catalog"."default",
  "domain_config_plugin_id" int4,
  "domain_id" int4,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."domain_configs" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of domain_configs
-- ----------------------------
BEGIN;
INSERT INTO "public"."domain_configs" VALUES (2, 'title-2.2', 'meta_tag-2', 'domain_config_site_name-2', NULL, 2, '2020-08-14 15:11:23.101+07', '2020-08-20 15:17:08.77+07');
INSERT INTO "public"."domain_configs" VALUES (6, 'title-4', 'domain_config_meta_tag-4', 'domain_config_site_name-4', NULL, 4, '2020-08-20 15:17:08.778+07', '2020-08-20 15:17:08.778+07');
INSERT INTO "public"."domain_configs" VALUES (1, 'title-1.1', 'domain_config_meta_tag-1', 'domain_config_site_name-1', NULL, 1, '2020-08-14 15:11:23.101+07', '2020-08-20 15:17:08.79+07');
INSERT INTO "public"."domain_configs" VALUES (5, 'title-3', 'domain_config_meta_tag-1', 'domain_config_site_name-1', NULL, 3, '2020-08-14 16:35:01.997+07', '2020-08-20 15:17:08.795+07');
INSERT INTO "public"."domain_configs" VALUES (7, 'new-title-1.1w', 'new-Meta-1.1w', 'new-Site-1.1w', NULL, 15, '2020-08-21 11:53:58.078+07', '2020-08-21 11:53:58.078+07');
COMMIT;

-- ----------------------------
-- Table structure for domain_plugins
-- ----------------------------
DROP TABLE IF EXISTS "public"."domain_plugins";
CREATE TABLE "public"."domain_plugins" (
  "id" int4 NOT NULL DEFAULT nextval('domain_plugins_id_seq'::regclass),
  "domain_id" int4,
  "plugin_id" int4,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."domain_plugins" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of domain_plugins
-- ----------------------------
BEGIN;
INSERT INTO "public"."domain_plugins" VALUES (36, 2, 1, '2020-08-20 15:17:08.758+07', '2020-08-20 15:17:08.758+07');
INSERT INTO "public"."domain_plugins" VALUES (37, 2, 2, '2020-08-20 15:17:08.758+07', '2020-08-20 15:17:08.758+07');
INSERT INTO "public"."domain_plugins" VALUES (38, 4, 1, '2020-08-20 15:17:08.762+07', '2020-08-20 15:17:08.762+07');
INSERT INTO "public"."domain_plugins" VALUES (39, 4, 5, '2020-08-20 15:17:08.762+07', '2020-08-20 15:17:08.762+07');
INSERT INTO "public"."domain_plugins" VALUES (40, 3, 1, '2020-08-20 15:17:08.764+07', '2020-08-20 15:17:08.764+07');
INSERT INTO "public"."domain_plugins" VALUES (41, 3, 2, '2020-08-20 15:17:08.764+07', '2020-08-20 15:17:08.764+07');
INSERT INTO "public"."domain_plugins" VALUES (42, 3, 3, '2020-08-20 15:17:08.764+07', '2020-08-20 15:17:08.764+07');
INSERT INTO "public"."domain_plugins" VALUES (43, 1, 1, '2020-08-20 15:17:08.766+07', '2020-08-20 15:17:08.766+07');
INSERT INTO "public"."domain_plugins" VALUES (44, 1, 2, '2020-08-20 15:17:08.766+07', '2020-08-20 15:17:08.766+07');
INSERT INTO "public"."domain_plugins" VALUES (45, 1, 3, '2020-08-20 15:17:08.766+07', '2020-08-20 15:17:08.766+07');
INSERT INTO "public"."domain_plugins" VALUES (46, 15, 1, '2020-08-21 11:53:58.07+07', '2020-08-21 11:53:58.07+07');
INSERT INTO "public"."domain_plugins" VALUES (47, 15, 5, '2020-08-21 11:53:58.07+07', '2020-08-21 11:53:58.07+07');
INSERT INTO "public"."domain_plugins" VALUES (48, 15, 6, '2020-08-21 11:53:58.07+07', '2020-08-21 11:53:58.07+07');
COMMIT;

-- ----------------------------
-- Table structure for host_domains
-- ----------------------------
DROP TABLE IF EXISTS "public"."host_domains";
CREATE TABLE "public"."host_domains" (
  "id" int4 NOT NULL DEFAULT nextval('host_domains_id_seq'::regclass),
  "host_domain_name" varchar(255) COLLATE "pg_catalog"."default",
  "host_id" int4,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL,
  "user_id" int4 NOT NULL
)
;
ALTER TABLE "public"."host_domains" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of host_domains
-- ----------------------------
BEGIN;
INSERT INTO "public"."host_domains" VALUES (1, 'host1', 3, '2020-08-13 13:15:46.56+07', '2020-08-13 13:15:46.56+07', 1);
INSERT INTO "public"."host_domains" VALUES (2, 'host2', 3, '2020-08-13 13:15:46.56+07', '2020-08-13 13:15:46.56+07', 1);
INSERT INTO "public"."host_domains" VALUES (3, 'host3', 3, '2020-08-13 13:15:46.56+07', '2020-08-13 13:15:46.56+07', 1);
INSERT INTO "public"."host_domains" VALUES (4, 'domain4', 3, '2020-08-17 11:05:24.696+07', '2020-08-17 11:05:24.696+07', 1);
INSERT INTO "public"."host_domains" VALUES (5, 'domains5', 3, '2020-08-17 11:05:24.696+07', '2020-08-17 11:05:24.696+07', 1);
INSERT INTO "public"."host_domains" VALUES (6, 'domains6', 3, '2020-08-17 11:05:24.696+07', '2020-08-17 11:05:24.696+07', 1);
INSERT INTO "public"."host_domains" VALUES (7, 'domain6', 3, '2020-08-20 14:43:16.599+07', '2020-08-20 14:43:16.599+07', 1);
INSERT INTO "public"."host_domains" VALUES (8, 'domains7', 3, '2020-08-20 14:43:16.599+07', '2020-08-20 14:43:16.599+07', 1);
INSERT INTO "public"."host_domains" VALUES (9, 'domains8', 3, '2020-08-20 14:43:16.599+07', '2020-08-20 14:43:16.599+07', 1);
INSERT INTO "public"."host_domains" VALUES (10, 'domains9', 3, '2020-08-20 14:43:16.599+07', '2020-08-20 14:43:16.599+07', 1);
INSERT INTO "public"."host_domains" VALUES (11, 'domain10', 3, '2020-08-20 14:43:46.716+07', '2020-08-20 14:43:46.716+07', 1);
INSERT INTO "public"."host_domains" VALUES (12, 'domains11', 3, '2020-08-20 14:43:46.716+07', '2020-08-20 14:43:46.716+07', 1);
INSERT INTO "public"."host_domains" VALUES (13, 'domains12', 3, '2020-08-20 14:43:46.716+07', '2020-08-20 14:43:46.716+07', 1);
INSERT INTO "public"."host_domains" VALUES (14, 'domains13', 3, '2020-08-20 14:43:46.716+07', '2020-08-20 14:43:46.716+07', 1);
INSERT INTO "public"."host_domains" VALUES (15, 'Domain-new-1', 3, '2020-08-21 11:44:32.762+07', '2020-08-21 11:44:32.762+07', 1);
INSERT INTO "public"."host_domains" VALUES (16, 'Domain-new-2', 3, '2020-08-21 11:44:32.762+07', '2020-08-21 11:44:32.762+07', 1);
INSERT INTO "public"."host_domains" VALUES (17, 'Domain-new-3', 3, '2020-08-21 11:44:32.762+07', '2020-08-21 11:44:32.762+07', 1);
COMMIT;

-- ----------------------------
-- Table structure for host_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."host_users";
CREATE TABLE "public"."host_users" (
  "id" int4 NOT NULL DEFAULT nextval('host_users_id_seq'::regclass),
  "host_user_username" varchar(255) COLLATE "pg_catalog"."default",
  "host_user_password" text COLLATE "pg_catalog"."default",
  "host_id" int4,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."host_users" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of host_users
-- ----------------------------
BEGIN;
INSERT INTO "public"."host_users" VALUES (2, 'dojeed', '$2a$08$SucSyzdluSCOobttMkMrbOmkhoE8DqNklVHmrixBaFEtFTHFcclHa', 3, '2020-08-20 15:41:50.864+07', '2020-08-20 15:41:50.864+07');
COMMIT;

-- ----------------------------
-- Table structure for hosts
-- ----------------------------
DROP TABLE IF EXISTS "public"."hosts";
CREATE TABLE "public"."hosts" (
  "id" int4 NOT NULL DEFAULT nextval('hosts_id_seq'::regclass),
  "host_name" varchar(255) COLLATE "pg_catalog"."default",
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."hosts" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of hosts
-- ----------------------------
BEGIN;
INSERT INTO "public"."hosts" VALUES (3, 'host-1', '2020-08-13 11:07:36.781+07', '2020-08-13 11:07:36.781+07');
COMMIT;

-- ----------------------------
-- Table structure for plugins
-- ----------------------------
DROP TABLE IF EXISTS "public"."plugins";
CREATE TABLE "public"."plugins" (
  "id" int4 NOT NULL DEFAULT nextval('plugins_id_seq'::regclass),
  "plugin_name" varchar(255) COLLATE "pg_catalog"."default",
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."plugins" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of plugins
-- ----------------------------
BEGIN;
INSERT INTO "public"."plugins" VALUES (1, 'Contact Form 7', '2020-08-13 14:49:24.835+07', '2020-08-13 14:49:24.835+07');
INSERT INTO "public"."plugins" VALUES (2, 'Yoast SEO', '2020-08-13 14:49:48.832+07', '2020-08-13 14:49:48.832+07');
INSERT INTO "public"."plugins" VALUES (3, 'Elementor Page Builder', '2020-08-13 14:49:58.167+07', '2020-08-13 14:49:58.167+07');
INSERT INTO "public"."plugins" VALUES (4, 'WooCommerce', '2020-08-13 14:50:07.636+07', '2020-08-13 14:50:07.636+07');
INSERT INTO "public"."plugins" VALUES (5, 'bbPress', '2020-08-13 14:50:27.378+07', '2020-08-13 14:50:27.378+07');
INSERT INTO "public"."plugins" VALUES (6, 'Jetpack by WordPress.com', '2020-08-13 14:50:41.603+07', '2020-08-13 14:50:41.603+07');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('"Users_id_seq"'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default",
  "password" text COLLATE "pg_catalog"."default",
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;
ALTER TABLE "public"."users" OWNER TO "o2gmtadMdo";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "public"."users" VALUES (1, 'dojeed', 'dojisjis22@gmail.com', '$2a$08$ZaN/7Km2iXW8xiWkomkcFebdaX5NT0o84G.7AyqIIOdLsHLusFH0y', '2020-08-17 11:00:17.624+07', '2020-08-17 11:00:17.624+07');
INSERT INTO "public"."users" VALUES (2, 'tawatchai', 'mrtawatchaimai@gmail.com', '$2a$08$Ps572b9yeal76qSa0HVCp.v8ZiIP4xPOc.WfTbfJ/jOVI1HUGofgm', '2020-08-17 13:56:56.869+07', '2020-08-17 13:56:56.869+07');
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."Users_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."domain_configs_id_seq"
OWNED BY "public"."domain_configs"."id";
SELECT setval('"public"."domain_configs_id_seq"', 8, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."domain_plugins_id_seq"
OWNED BY "public"."domain_plugins"."id";
SELECT setval('"public"."domain_plugins_id_seq"', 49, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."host_domains_id_seq"
OWNED BY "public"."host_domains"."id";
SELECT setval('"public"."host_domains_id_seq"', 18, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."host_users_id_seq"
OWNED BY "public"."host_users"."id";
SELECT setval('"public"."host_users_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."hosts_id_seq"
OWNED BY "public"."hosts"."id";
SELECT setval('"public"."hosts_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."plugins_id_seq"
OWNED BY "public"."plugins"."id";
SELECT setval('"public"."plugins_id_seq"', 7, true);

-- ----------------------------
-- Primary Key structure for table SequelizeMeta
-- ----------------------------
ALTER TABLE "public"."SequelizeMeta" ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY ("name");

-- ----------------------------
-- Primary Key structure for table domain_configs
-- ----------------------------
ALTER TABLE "public"."domain_configs" ADD CONSTRAINT "domain_configs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table domain_plugins
-- ----------------------------
ALTER TABLE "public"."domain_plugins" ADD CONSTRAINT "domain_plugins_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table host_domains
-- ----------------------------
ALTER TABLE "public"."host_domains" ADD CONSTRAINT "host_domains_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table host_users
-- ----------------------------
ALTER TABLE "public"."host_users" ADD CONSTRAINT "host_users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table hosts
-- ----------------------------
ALTER TABLE "public"."hosts" ADD CONSTRAINT "hosts_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table plugins
-- ----------------------------
ALTER TABLE "public"."plugins" ADD CONSTRAINT "plugins_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("id");
