'use strict';
module.exports = function (sequelize, DataTypes) {
  const Host_users = sequelize.define('host_users', {
    host_user_username: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_user_username in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'host_user_username more than 1 character but no more than 50 characters.',
        },
      }
    },
    host_user_password: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_user_password in not equal null.',
        }
      }
    },
    host_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'hosts',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_id in not equal null.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Host_users.associate = function (models) {
    // associations can be defined here
    Host_users.belongsTo(models.hosts, { foreignKey: 'host_id' });
  };
  return Host_users;
};