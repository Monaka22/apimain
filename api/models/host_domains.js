'use strict';
module.exports = function (sequelize, DataTypes) {
  const Host_domains = sequelize.define('host_domains', {
    host_domain_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_domain_name in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'host_domain_name more than 1 character but no more than 50 characters.',
        },
      }
    },
    host_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'hosts',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_id in not equal null.',
        },
      }
    },
    host_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'users',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'users in not equal null.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Host_domains.associate = function (models) {
    // associations can be defined here
    Host_domains.hasMany(models.domain_configs, { foreignKey: 'domain_id' });
    Host_domains.hasMany(models.domain_plugins, { foreignKey: 'domain_id' });
    Host_domains.belongsTo(models.hosts, { foreignKey: 'host_id' });
    Host_domains.belongsTo(models.users, { foreignKey: 'user_id' });
  };
  return Host_domains;
};