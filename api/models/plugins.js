'use strict';
module.exports = function (sequelize, DataTypes) {
  const Plugin = sequelize.define('plugins', {
    plugin_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'plugin_name in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'plugin_name more than 1 character but no more than 50 characters.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Plugin.associate = function (models) {
    // associations can be defined here
    Plugin.hasMany(models.domain_plugins, { foreignKey: 'plugin_id' });
  };
  return Plugin;
};
