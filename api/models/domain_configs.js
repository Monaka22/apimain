'use strict';
module.exports = function (sequelize, DataTypes) {
  const Domain_configs = sequelize.define('domain_configs', {
    domain_config_title: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'domain_config_title in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'domain_config_title more than 1 character but no more than 50 characters.',
        },
      }
    },
    domain_config_meta_tag: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'domain_config_meta_tag in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'domain_config_meta_tag more than 1 character but no more than 50 characters.',
        },
      }
    },
    domain_config_site_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'domain_config_site_name in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'domain_config_site_name more than 1 character but no more than 50 characters.',
        },
      }
    },
    domain_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'domain_configs',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'domain_id in not equal null.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Domain_configs.associate = function (models) {
    // associations can be defined here
    Domain_configs.belongsTo(models.host_domains, { foreignKey: 'domain_id' });
  };
  return Domain_configs;
};