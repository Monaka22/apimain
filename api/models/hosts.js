'use strict';
module.exports = function (sequelize, DataTypes) {
  const Hosts = sequelize.define('hosts', {
    host_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'host_name in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'host_name more than 1 character but no more than 50 characters.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Hosts.associate = function (models) {
    // associations can be defined here
    Hosts.hasMany(models.host_users, { foreignKey: 'host_id' });
    Hosts.hasMany(models.host_domains, { foreignKey: 'host_id' });
  };
  return Hosts;
};
