'use strict';
module.exports = function (sequelize, DataTypes) {
  const Domain_plugins = sequelize.define('domain_plugins', {
    domain_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'domains',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'domain_id in not equal null.',
        },
      }
    },
    plugin_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'plugins',
        key: 'id'
      },
      allowNull: false,
      validate: {
        notNull: {
          msg: 'plugin_id in not equal null.',
        },
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Domain_plugins.associate = function (models) {
    // associations can be defined here
    Domain_plugins.belongsTo(models.host_domains, { foreignKey: 'domain_id' });
    Domain_plugins.belongsTo(models.plugins, { foreignKey: 'plugin_id' });
  };
  return Domain_plugins;
};