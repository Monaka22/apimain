'use strict';
module.exports = function (sequelize, DataTypes) {
  const Users = sequelize.define('users', {
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'name in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'name more than 1 character but no more than 50 characters.',
        },
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'email in not equal null.',
        },
        len: {
          args: [1, 50],
          msg: 'email more than 1 character but no more than 50 characters.',
        },
      }
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'password in not equal null.',
        }
      }
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    }
  }, {
    force: true,
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  });
  Users.associate = function (models) {
    // associations can be defined here
    Users.hasMany(models.host_domains, { foreignKey: 'user_id' });
  };
  return Users;
};
