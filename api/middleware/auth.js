const { firebase } = require('../config/firebase.js');
const { Op, literal } = require('sequelize');

const checkToken = (req, res, next) => {
    if (req.headers.authorization) {
        const token = req.headers.authorization.split(' ')[1];
        firebase.auth().verifyIdToken(token)
            .then((decodeedToken) => {
                // let uid = decodeedToken.uid;
                req.uid_token = decodeedToken.uid;
                next()
            })
            .catch((error) => {
                error.message = 'กรุณาล็อกอินใหม่อีกครั้ง';
                next({
                    status: 401,
                    message: 'กรุณาล็อกอินใหม่อีกครั้ง',
                    error: error
                });
            })
    } else {
        let error = new Error('กรุณาล็อกอินใหม่อีกครั้ง');
        error.status = 401
        error.error = 'No header.'
        next(error);
    }
};

const refreshToken = (uid) => {
    firebase.auth().revokeRefreshTokens(uid)
        .then(() => {
            console.log('then 1', uid);
            return firebase.auth().getUser(uid);
        })
        .then((userRecord) => {
            console.log('then 2', userRecord);
            return new Date(userRecord.tokensValidAfterTime).getTime() / 1000;
        })
        .then((timestamp) => {
            console.log('then 3', timestamp);
            console.log('Tokens revoked at: ', timestamp);
        });
};

module.exports = {
    checkToken,
    refreshToken
};