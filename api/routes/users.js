const express = require('express');
const router = express.Router();
const Schemas = require('../schemas');
const auth = require('../middleware/auth') 

router.post('/register', function (req, res, next) {
    Schemas.Users.createUser(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.post('/login', function (req, res, next) {
    Schemas.Users.Login(req, '').then(data => {
        res.status(201).json({
            status: true,
            message: 'เข้าสู่ระบบสำเร็จ',
            data
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.get('/',
auth.checkToken,
function (req, res, next) {
    Schemas.Users.getUsers(req, '').then(data => {
        res.status(200).json({
            status: true,
            data
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});

module.exports = router;