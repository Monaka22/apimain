const express = require('express');
const router = express.Router();
const Schemas = require('../schemas');
const auth = require('../middleware/auth') 

router.post('/host-create',
auth.checkToken,
 function (req, res, next) {
    Schemas.hostUser.createHost(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.post('/user-create', 
auth.checkToken,
function (req, res, next) {
    Schemas.hostUser.createHostUser(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.post('/domain-create', 
auth.checkToken,
function (req, res, next) {
    Schemas.hostUser.createHostDomains(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.get('/host-all',
auth.checkToken,
 function (req, res, next) {
    Schemas.hostUser.getAllHost(req, '').then(data => {
        res.status(201).json({
            status: true,
            message: 'สำเร็จ',
            data:data
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.get('/domain/:user_id/:offset/:length',
auth.checkToken,
 function (req, res, next) {
    Schemas.hostUser.countHostDomainsAll(req, '').then(total => {
        Schemas.hostUser.getHostDomains(req, '').then(data => {
            res.status(200).json({
                status: true,
                message: 'สำเร็จ',
                data:data,
                total
            })
        }).catch(error => {
            error.status = 400;
            next(error);
        });
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});


module.exports = router;