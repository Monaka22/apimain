const router = require('express').Router();
const { momentTZ } = require('../config/moment.js');
//routes
const host = require('./host');
const users = require('./users');
const plugin = require('./plugins');
const config = require('./config');
router.get('/', function (req, res, next) {
    // Schemas.Queue.dummyCreatePoints(21, 1);
    res.status(200).json({
      status: true,
      message: 'Wellcome to the backend API',
      test1: new Date().getTime(),
      test2: momentTZ().unix(),
      test3: momentTZ().valueOf(),
    });
  });

  module.exports = function (app) {
    app.use('/api/', router);
    app.use('/api/users', users);
    app.use('/api/host', host);
    app.use('/api/plugin', plugin);
    app.use('/api/config', config);
  };