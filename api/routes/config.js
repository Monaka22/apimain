const express = require('express');
const router = express.Router();
const Schemas = require('../schemas');
const auth = require('../middleware/auth') 

router.post('/create',
auth.checkToken,
 function (req, res, next) {
    Schemas.Config.configPlugin(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ'
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.post('/get',
auth.checkToken,
 function (req, res, next) {
    Schemas.Config.getDomainConfigPlugin(req, '').then(data => {
        res.status(200).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
            data
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});

module.exports = router;