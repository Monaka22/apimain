const express = require('express');
const router = express.Router();
const Schemas = require('../schemas');
const auth = require('../middleware/auth') 

router.post('/create', 
auth.checkToken,
function (req, res, next) {
    Schemas.Plugins.createpPlugin(req, '').then(logs => {
        res.status(201).json({
            status: true,
            message: 'สร้างข้อมูลสำเร็จ',
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});
router.get('/all', 
auth.checkToken,
function (req, res, next) {
    Schemas.Plugins.getHostPlugin(req, '').then(data => {
        res.status(200).json({
            status: true,
            message: 'สำเร็จ',
            data:data
        })
    }).catch(error => {
        error.status = 400;
        next(error);
    });
});

module.exports = router;