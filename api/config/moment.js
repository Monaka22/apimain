const moment = require('moment-timezone');
moment.tz.setDefault('Asia/Bangkok');
exports.momentTZ = moment;