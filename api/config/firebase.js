const firebase = require('firebase-admin');
//use firebase 
const serviceAccount = require('../serviceAccountKey.json');

if (!firebase.apps.length) {
  firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: process.env.DATABASEURL
  });
}

exports.firebase = firebase;