const { v1: uuidv1 } = require('uuid');
const { v4: uuidv4 } = require('uuid');
const { momentTZ } = require('./moment.js');

const created = () => {
  return uuidv1({
    msecs: momentTZ().valueOf(),
  })
}

const createdV4 = () => {
  return uuidv4()
}

module.exports = {
  created,
  createdV4
}