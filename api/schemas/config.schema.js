const Models = require("../models");
const { Op, literal } = require("sequelize");
const countHostDomainsAll = () => {
  return new Promise((resolve, reject) => {
      Models.domain_configs.count()
          .then((total) => {
              resolve(total);
          })
          .catch(error => {
              reject(error);
          });
  });
}
const configPlugin = (value, fileImage) => {
  return new Promise((resolve, reject) => {
    let data = value.body.data;
    data.forEach(async (element) => {
      let pluginArray = []
      await element.plugin.forEach(plugin_id=>{
        pluginArray.push({
          domain_id:element.domain_id,
          plugin_id : plugin_id
        })
      })
      await Models.domain_plugins.destroy({ where: { domain_id:element.domain_id  }})
     let bulkCreateDomainPlugins = Models.domain_plugins
        .bulkCreate(pluginArray, {
          fields: ["domain_id", "plugin_id"],
        })
        .then((host) => {
          resolve(host);
        })
        .catch((error) => {
          reject(error);
        });
        let createDomainConfig  =  Models.domain_configs.count({
          where: { domain_id: element.domain_id }
        })
          .then((total) => {
              if (total > 0) {
                Models.domain_configs
                    .update(
                      {
                        domain_config_title: element.domain_config_title,
                        domain_config_meta_tag: element.domain_config_meta_tag,
                        domain_config_site_name: element.domain_config_site_name,
                      },{
                        where: {
                          domain_id: element.domain_id,
                        }
                    }
                    )
                    .then((doc) => {
                      resolve(doc);
                    })
                    .catch((error) => {
                      reject(error);
                    });
              }else{
                  Models.domain_configs
                    .create(
                      {
                        domain_config_title: element.domain_config_title,
                        domain_config_meta_tag: element.domain_config_meta_tag,
                        domain_config_site_name: element.domain_config_site_name,
                        domain_id: element.domain_id,
                      },
                      {
                        fields: ["domain_config_title","domain_config_meta_tag","domain_config_site_name","domain_id"],
                      }
                    )
                    .then((doc) => {
                      resolve(doc);
                    })
                    .catch((error) => {
                      reject(error);
                    });
              }
          })
          .catch(error => {
              reject(error);
          });
       await Promise.all([
          bulkCreateDomainPlugins,createDomainConfig
        ])
    });
  });
};

const getDomainConfigPlugin = (req, res) => {
  return new Promise((resolve, reject) => {
    Models.domain_configs
      .findAll(
        {
          attributes: ["id", "domain_config_title", "domain_config_meta_tag","domain_config_site_name","domain_id"],
          where: { domain_id: req.body.domain_id }
        }
      )
      .then((config) => {
          Models.domain_plugins
            .findAll(
              {
                attributes: ["domain_id", "plugin_id"],
                where: { domain_id: req.body.domain_id }
              }
            )
            .then((data) => {
              let plugin = []
              data.forEach(doc => {
                plugin.push(doc)
              });
              resolve({plugin,config});
            })
            .catch((error) => {
              reject(error);
            });
      })
      .catch((error) => {
        reject(error);
      });
  });
};
module.exports = {
  configPlugin,
  getDomainConfigPlugin,
  countHostDomainsAll
};
