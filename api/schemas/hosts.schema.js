const Models = require("../models");
const { Op, literal } = require("sequelize");
const bcrypt = require("bcryptjs");

const createHost = (value, fileImage) => {
  return new Promise((resolve, reject) => {
    Models.hosts
      .create(
        {
          host_name: value.body.host_name,
        },
        {
          fields: ["host_name"],
        }
      )
      .then((host) => {
        resolve(host);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const getAllHost = (req, res) => {
  return new Promise((resolve, reject) => {
    Models.hosts
      .findAll()
      .then((host) => {
        resolve(host);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const createHostUser = (value, fileImage) => {
  value.body.host_user_password = bcrypt.hashSync(value.body.host_user_password, 8);
  return new Promise((resolve, reject) => {
    Models.host_users
      .create(
        {
          host_user_username: value.body.host_user_username,
          host_user_password: value.body.host_user_password,
          host_id: value.body.host_id,
        },
        {
          fields: ["host_user_username", "host_user_password", "host_id"],
        }
      )
      .then((host) => {
        resolve(host);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const createHostDomains = (value, fileImage) => {
  return new Promise((resolve, reject) => {
    let domainArray = value.body.domains;
    for (let index = 0; index < domainArray.length; index++) {
      domainArray[index].user_id = value.body.user_id
    }
    if (domainArray.length > 0) {
      Models.host_domains
        .bulkCreate(domainArray, {
          fields: ["host_domain_name", "host_id","user_id"],
        })
        .then((host) => {
          resolve(host);
        })
        .catch((error) => {
          reject(error);
        });
    } else {
      reject({
        status: false,
        message: "notNull Violation: host_domain_name.length > 0.",
        error: {
          name: "SequelizeValidationError",
          errors: [
            {
              message: "host_domain_name.length > 0.",
              type: "notNull Violation",
              path: "host_domain_name",
            },
          ],
          status: 400,
        },
      });
    }
  });
};
const countHostDomainsAll = () => {
  return new Promise((resolve, reject) => {
      Models.host_domains.count()
          .then((total) => {
              resolve(total);
          })
          .catch(error => {
              reject(error);
          });
  });
}
const getHostDomains = (req, res) => {
  return new Promise((resolve, reject) => {
    Models.host_domains
      .findAll({
        where :{ user_id : req.params.user_id},
        attributes: ["id", "host_domain_name", "host_id"],
        offset: req.params.offset,
        limit: req.params.length,
        order:[
            ['id','asc']
        ]
      })
      .then((host) => {
        resolve(host);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
module.exports = {
  createHost,
  getAllHost,
  createHostUser,
  createHostDomains,
  getHostDomains,
  countHostDomainsAll
};
