const Models = require('../models');
const { Op, literal } = require('sequelize');
const bcrypt = require("bcryptjs");
const { firebase } = require('../config/firebase');

const getUsers = () => {
    return new Promise((resolve, reject) => {
        Models.users.findAll({
            attributes:['email','name']
        })
            .then((users) => {
                resolve(users);
            })
            .catch(error => {
                reject(error);
            });
    });
}

const countUsersAll = () => {
    return new Promise((resolve, reject) => {
        Models.users.count()
            .then((users) => {
                resolve(users);
            })
            .catch(error => {
                reject(error);
            });
    });
}
const Login = (value, attributes) => {
    return new Promise(async(resolve, reject) => {
        let result = await Models.users.findOne({ where: { email: value.body.email } });
            if (result != null) {
                    if (bcrypt.compareSync(value.body.password, result.password)) {
                        resolve({
                            result: "login success",
                            message: result
                        });
                        } else {
                        reject({ result: "login faild", message: "Incorrect password" });
                        }
            } else {
                reject({ result: "login faild", message: "Incorrect email" });
            }
    });
}

const createUser = (value, fileImage) => {
    return new Promise((resolve, reject) => {
        firebase.auth().createUser({
            email : value.body.email,
            password: value.body.password,
            emailVerified: false,
            displayName: value.body.name,
            disabled: false
        })
            .then(() => {
                value.body.password = bcrypt.hashSync(value.body.password, 8);
                Models.users.create({
                    name:value.body.name,
                    email : value.body.email,
                    password: value.body.password
                }, {
                    fields: ['email', 'name','password']
                }).then((user) => {
                    resolve(user);
                }).catch((error) => {
                    reject(error);
                });
            })
            .catch((error) => {
                reject(error);
            });
    });
}

const updateUser = (value, uid) => {
    return new Promise((resolve, reject) => {
        Models.users.update({
            name:value.body.name,
            email : value.body.email,
            password: value.body.password
        }, {
            where: {
                id : value.body.id
            }
        }).then((user) => {
                firebase.auth().updateUser(uid, {
                    email: value.body.email,
                    displayName: value.body.name,
                })
                    .then((userRecord) => {
                        resolve(userRecord)
                    })
                    .catch((error) => {
                        reject(error);
                    });
        }).catch((error) => {
            reject(error);
        });
    });
}

const deleteUser = (value,uid) => {
    return new Promise((resolve, reject) => {
        Models.users.destroy({
            where: {
                id: value.body.id
            }
        })
            .then(() => {
                firebase.auth().deleteUser(uid)
                    .then(() => {
                        resolve('User destroyed');
                    })
                    .catch((error) => {
                        reject(error);
                    });
            }).catch((error) => {
                reject(error);
            });
    });
}

module.exports = {
    countUsersAll,
    getUsers,
    Login,
    createUser,
    updateUser,
    deleteUser,
};
