const Models = require("../models");
const { Op, literal } = require("sequelize");

const createpPlugin = (value, fileImage) => {
  return new Promise((resolve, reject) => {
    console.log(value.body)
    Models.plugins.create(
        {
          plugin_name: value.body.plugin_name,
        },
        {
          fields: ["plugin_name"],
        }
      )
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const getHostPlugin = (req, res) => {
  return new Promise((resolve, reject) => {
    Models.plugins
      .findAll()
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
module.exports = {
  createpPlugin,
  getHostPlugin,
};
