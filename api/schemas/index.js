'use strict';
const Users = require('./users.schema');
const hostUser = require('./hosts.schema');
const Plugins = require('./plugins.schema');
const Config = require('./config.schema');

module.exports = {
    Users,
    hostUser,
    Plugins,
    Config
};