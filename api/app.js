const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes');
const app = express();
// const db = require("./models");
// db.sequelize.sync();

app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(cookieParser());

app.use(cors({
  origin: true,
  credentials: true,
  optionsSuccessStatus: 200
}))

routes(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let error = new Error("Not Found");
  error.status = 404;
  next(error);
});


app.use(function (error, req, res, next) {
  res.status(error.status || 500);
  if (error.status == 422) {
    res.json({
      status: false,
      message: error.message,
      validate: error.validate,
    });
  } else {
    console.error("error ", error);
    res.json({
      status: false,
      message: error.message,
      // error: (app.get('env') === 'development') ? error : {}
      error: error,
    });
  }
});

module.exports = app;